const express = require("express");
const app = express();
const router = express.Router();

const fs = require("fs");

app.use(express.json());

router.get("/", (req, res) => {
  fs.readdir(process.cwd(), function (err, folders) {
    const existingFolder = [];
    folders.forEach((folder) => {
      if (folder == "files") {
        existingFolder.push(folder);
      }
    });
    if (existingFolder.length > 0) {
      fs.readdir("files", function (err, files) {
        if (err) {
          res.status(500).send({
            message: "Server error",
          });
        } else {
          const arrayOfFiles = [];
          files.forEach((file) => {
            arrayOfFiles.push(file);
          });
          const arrayofFilesFiltered = arrayOfFiles.filter((value)=>value != '.gitkeep');
          res.status(200).send({ message: "Success", files: arrayofFilesFiltered });
        }
      });
    } else if (existingFolder.length == 0) {
      res.status(400).send({
        message: "Client error",
      });
    } else if (err) {
      res.status(500).send({
        message: "Server error",
      });
    }
  });
});

router.get("/:filename", (req, res) => {
  const regex = new RegExp("[^.]+$");
  const extension = req.params.filename.match(regex);

  fs.readdir(process.cwd(), function (err, folders) {
    const existingFolder = [];
    folders.forEach((folder) => {
      if (folder == "files") {
        existingFolder.push(folder);
      }
    });
    if (existingFolder.length > 0) {
      fs.readFile(
        `files/${req.params.filename}`,
        "utf8",
        function (err, content) {
          if (err) {
            res
              .status(400)
              .send({
                message: `No file with ${req.params.filename} filename found`,
              });
          } else {
            res
              .status(200)
              .send({
                message: "Success",
                filename: req.params.filename,
                content: content,
                extension: extension.join(),
                uploadedDate: fs.statSync(`files/${req.params.filename}`)
                  .birthtime,
              });
          }
        }
      );
    } else if (existingFolder.length == 0) {
      res.status(400).send({
        message: "Client error",
      });
    } else if (err) {
      res.status(500).send({
        message: "Server error",
      });
    }
  });
});

router.post("/", (req, res) => {
  const reqexp =
    /([a-zA-Z0-9\s_\\.\-\(\):])+(.log|.txt|.json|.yaml|.xml|.js)$/i;
  const fileValidation = reqexp.test(req.body.filename);
  if (!req.body.filename) {
    res.status(400).send({ message: "Please specify 'filename' parameter" });
  } else if (!fileValidation) {
    res.status(400).send({ message: "Sorry, unvalid  file extension" });
  } else if (!req.body.content) {
    res.status(400).send({ message: "Please specify 'content' parameter" });
  } else {
    fs.readdir("files", function (err, files) {
      if (err) {
        res.status(500).send({ message: "Server error" });
      } else {
        const existingFile = [];
        files.forEach((value) => {
          if (value == req.body.filename) {
            existingFile.push(value);
          }
        });
        if (existingFile.length > 0) {
          res.status(400).send({ message: "Sorry, this file already exists" });
        } else {
          fs.writeFile(
            `files/${req.body.filename}`,
            req.body.content,
            function (err) {
              if (err) {
                res.status(500).send({ message: "Server error" });
              } else {
                res.status(200).send({ message: "File created successfully" });
              }
            }
          );
        }
      }
    });
  }
});

module.exports = router;
