const express = require('express');
const app = express();
const morgan = require('morgan')

app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use(morgan('common'))

const apiFileRouter = require('./routes/apiFiles')


app.use('/api/files', apiFileRouter)


app.listen(8080)